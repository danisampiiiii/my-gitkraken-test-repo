#include <iostream>
#include <vector>
#include <cinttypes>
#include <algorithm>

int main(void)
{
	int iTestCaseCount;
	std::cin >> iTestCaseCount;
	
	while(iTestCaseCount--)
	{
		uint32_t uiDominoCount;
		std::cin >> uiDominoCount;
		
		std::vector<uint32_t> aHeight(uiDominoCount);
		for(uint32_t ui = 0; ui < uiDominoCount; ++ui)
		{
			std::cin >> aHeight[ui];
		}
		
		uint32_t uiLastFallPos = 0;
		for(uint32_t ui = 0; ui <= uiLastFallPos; ++ui)
		{
			uint32_t uiNextRange = ui + aHeight[ui] - 1;
			uiLastFallPos = std::max(uiLastFallPos, uiNextRange);
			if(uiLastFallPos >= uiDominoCount) { break; }
		}
		std::cout << std::min(uiDominoCount, uiLastFallPos + 1u) << std::endl;
	}
	
	return 0;
}
